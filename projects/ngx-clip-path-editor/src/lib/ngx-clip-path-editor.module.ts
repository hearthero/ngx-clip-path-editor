import { NgModule } from '@angular/core';
import {ClipPathEditorComponent} from './ngx-clip-path-editor.component';
import {MaskSvgPathPipe} from './pipes/mask-svg-path.pipe';
import {MaskClipPathPipe} from './pipes/mask-clip-path.pipe';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    ClipPathEditorComponent,
    MaskSvgPathPipe,
    MaskClipPathPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ClipPathEditorComponent
  ]
})
export class ClipPathEditorModule { }
