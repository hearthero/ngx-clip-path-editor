import { Pipe, PipeTransform } from '@angular/core';
import {ImageMaskPoint} from '../model/image-mask-point';

/**
 * Converts a point array to a clip path string
 */
@Pipe({
  name: 'maskClipPath',
  pure: true
})
export class MaskClipPathPipe implements PipeTransform {

  transform(value: ImageMaskPoint[]): string {

    if (value.length === 0) return '';

    let path = 'polygon(';

    for (const point of value) {
      path += (point.x + '% ') + (point.y + '%, ');
    }

    return path.slice(0, -2) + ')';
  }

  /**
   * Static function to retrieve points as a clip path
   * @param points
   */
  static getMaskClipPath(points: ImageMaskPoint[]) {
    let path = '';

    for (const point of points) {
      path += (point.x + '% ') + (point.y + '%, ');
    }

    return path.slice(0, -2);
  }

}
