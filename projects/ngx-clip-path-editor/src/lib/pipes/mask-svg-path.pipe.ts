import { Pipe, PipeTransform } from '@angular/core';
import {ImageMaskPoint} from '../model/image-mask-point';

/**
 * Converts a point array to a svg path string
 */
@Pipe({
  name: 'maskSvgPath',
  pure: true
})
export class MaskSvgPathPipe implements PipeTransform {

  transform(value: ImageMaskPoint[], svg: HTMLElement): string {

    if (value.length === 0) return '';

    const absoluteCopy = [...value].map((point) => {
      return {
        x: svg.clientWidth * (point.x / 100),
        y: svg.clientHeight * (point.y / 100)
      }
    });

    let path = 'M' + (absoluteCopy[0].x + ' ') + (absoluteCopy[0].y+ ' ');

    for (let i = 1; i < absoluteCopy.length; i++) {
      path += 'L ' + (absoluteCopy[i].x + ' ') + (absoluteCopy[i].y + ' ');
    }

    return path + 'Z';
  }

}
