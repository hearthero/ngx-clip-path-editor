/**
 * Simple representation of a point
 */
export interface ImageMaskPoint {
  x: number,
  y: number,
}
