import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component, EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  Output, SimpleChanges
} from '@angular/core';
import {debounceTime, distinctUntilChanged, fromEvent, Subscription} from 'rxjs';
import {MaskClipPathPipe} from './pipes/mask-clip-path.pipe';
import {ImageMaskPoint} from './model/image-mask-point';

@Component({
  selector: 'ngx-clip-path-editor',
  templateUrl: './ngx-clip-path-editor.component.html',
  styleUrls: ['./ngx-clip-path-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClipPathEditorComponent implements OnDestroy, OnChanges {

  @HostBinding('class.ready') private isReady = false;
  private __points: ImageMaskPoint[] = [];
  private resize$: Subscription;

  @Input() src: string | undefined;
  @Output() OnChange = new EventEmitter<string>();

  // Optional Input Settings

  @HostBinding('attr.disabled') @Input() disabled = false;

  @Input() mask: string | undefined;

  @Input() visualizeMask = true;
  @Input() visualizePath = true;
  @Input() visualizePoints = true;

  width = 0;
  height = 0;

  get points() {
    return this.__points;
  }
  set points(points: ImageMaskPoint[]) {
    this.__points = points;
    if (this.src) this.ref.detectChanges();
    this.OnChange.emit(this.getPath());
  }

  constructor(private ref: ChangeDetectorRef) {
    this.ref.detach();
    this.resize$ = fromEvent(window, 'resize').pipe(
      distinctUntilChanged(), debounceTime(50)
    ).subscribe(() => this.recreatePoints());
  }

  ngOnDestroy(): void {
    this.resize$.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['mask']) {
      this.points = [];
      if (this.src) this.parseMaskClipPath(changes['mask'].currentValue);
    }
    if (changes['src']) {
      this.isReady = false;
      this.ref.detectChanges();
    }
    if (changes['visualizeMask'] || changes['visualizePath'] || changes['visualizePoints']) {
      this.recreatePoints();
    }
  }

  clear() {
    this.points = [];
    this.mask = '';
  }

  /**
   * Sets the ready state to true and changes component visibility.
   * Gets called when an image source changes and the load event triggers
   */
  setReady($event: Event): void {
    const target = $event.target as HTMLImageElement;
    this.width = target.naturalWidth;
    this.height = target.naturalHeight;
    this.isReady = true;
    this.points = [];
    if (this.mask) this.parseMaskClipPath(this.mask);
  }

  /**
   * Adds a point to the points array
   * @param ev - the click event
   * @param mask - A reference to the SVG element
   */
  addPoint(ev: any, mask: HTMLElement): void {

    if (this.disabled) return;

    const relativePoint = {
      x: this.roundFloat(ev.layerX / mask.clientWidth) * 100,
      y: this.roundFloat(ev.layerY / mask.clientHeight) * 100
    }

    this.points = [...this.points, relativePoint];
  }

  /**
   * Removes a point from the points array
   * @param ev - the click event
   * @param i - the index of the point
   */
  removePoint(ev: MouseEvent, i: number): void {
    ev.stopPropagation();
    if (this.disabled) return;
    this.points.splice(i, 1);
    this.recreatePoints();
  }

  /**
   * Returns a clip path as string
   */
  getPath(): string {
    return MaskClipPathPipe.getMaskClipPath(this.points);
  }

  /**
   * Parses a mask clip path and fills the points array
   * @param path - A clipping path (only polygonal values are accepted)
   */
  parseMaskClipPath(path: string): void {
    const coordinates = path.replace(/[%()]|polygon/g, '').split(', ');

    for (const coordinate of coordinates) {
      const trimmedCoordinate = coordinate.split(' ');
      this.points.push({
        x: parseFloat(trimmedCoordinate[0]),
        y: parseFloat(trimmedCoordinate[1])
      });
    }
    this.recreatePoints();
  }

  /**
   * Recreates points for Change Detection.
   * An empty concat is faster than using a spread operator
   * (even though it should only really matter on VERY large arrays)
   * @private
   */
  private recreatePoints(): void {
    this.points = ([] as ImageMaskPoint[]).concat(this.points);
  }

  /**
   * Helper function for rounding floats
   *
   * @param num - the number to round
   * @param decimalPlaces - decimal places to round to
   * @private
   */
  private roundFloat(num: number, decimalPlaces=3): number {
    const p = Math.pow(10, decimalPlaces);
    const n = (num * p) * (1 + Number.EPSILON);
    return Math.round(n) / p;
  }

}
