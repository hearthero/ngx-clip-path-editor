/*
 * Public API Surface of ngx-clip-path-editor
 */

export * from './lib/ngx-clip-path-editor.component';
export * from './lib/ngx-clip-path-editor.module';
