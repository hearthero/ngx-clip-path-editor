# NGX ClipPath Editor

`ngx-clip-path-editor` is an Angular component for editing a CSS `clip-path` visually within an image element. It is customizable, responsive, lightweight and heavily [Change Detection](https://angular.io/guide/change-detection) optimized.

A demo can be found on [Stackblitz](https://stackblitz.com/edit/ngx-clip-path-editor-demo).

## About

If you've ever read a print magazine and compared the layout and style with typical websites you might have spotted some noticeable differences. When it comes to typography CSS got a lot of great features over the last couple of years. Furthermore, in terms of layout possibilities we are pretty much on par with print media nowadays. However, images on the web are typically just rectangular blocks and people barely get creative with fancy image layouts. This is the case because most editors don't cover options for stylizing pretty text flows around non-rectangular images - even though we already have the necessary technology.

Properties like [clip-path](https://developer.mozilla.org/en-US/docs/Web/CSS/clip-path) and [shape-outside](https://developer.mozilla.org/en-US/docs/Web/CSS/shape-outside) are widely supported and grant us the opportunity to create fancy layouts that don't evoke the impression that we are just staring at a bunch of rectangular blocks.

Some years ago I tried using these properties within a blog and people truly appreciated the more dynamic text flows. However, manually adding these properties to images was a tedious process. That's the reason I decided to create an editor component for creating clip paths. With an easy-to-use component everyone can create better layouts in no time.

## Prerequisites

- Angular >= 14
- RxJS 7

## Inputs & Outputs

| Input | Type | - | Description |
|--|--|--|--|
| src | `string` | Required | Sets the source of the image |
| mask | `string` | Optional | A clip-path string. If the mask Input is set the string will be parsed and visualized. |
| disabled | `boolean` | Optional | If set to `true` no points can be added or removed. |
| visualizeMask | `boolean` | Optional | If set to `false` the image background mask will not be shown. |
| visualizePath | `boolean` | Optional | If set to `false` the visualization of the path will not be shown. |
| visualizePoints | `boolean` | Optional | If set to `false` no points will be shown. |

The `OnChange` Event gets triggered whenever a new point gets added, an old point gets removed or the window resizes. It emits a clip-path string.

Triggering `clear` will clear all points.


## CSS Variables

The component makes heavy use of [custom CSS properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) which makes it easy to customize certain aspect via the [Shadow DOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM). The component also uses the [BEM](https://en.bem.info/methodology/quick-start/) approach so conflicts shouldn't occur when running without [ViewEncapsulation](https://angular.io/api/core/ViewEncapsulation).

| Variable | Description |
|--|--|
| `--mask-opacity` | Regulates the opacity of the background mask |
| `--mask-z-index` | The z-index value for the SVG element |
| `--path-unit` | A base unit size for the path and points  |
| `--path-opacity` | The default opacity for the path  |
| `--point-opacity` | The default opacity for points |

